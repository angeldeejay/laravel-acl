<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Contracts\Profile as ProfileContract;
use Laravel\Acl\Contracts\Role as RoleContract;
use Laravel\Acl\Contracts\Permission as PermissionContract;

class CreateProfile extends Command
{
    /**
     * @var string
     */
    protected $description = 'Create a profile';

    /**
     * @var string
     */
    protected $signature = 'permission:create-profile
        {name : The name of the profile}
        {guard? : The name of the guard}
        {permissions? : A list of permissions to assign to the profile, separated by | };
        {roles? : A list of roles to assign to the profile, separated by | }';

    public function handle()
    {
        $profileClass = app(ProfileContract::class);
        $profile      = $profileClass::findOrCreate($this->argument('name'), $this->argument('guard'));
        $profile->givePermissionTo($this->makePermissions($this->argument('permissions')));
        $profile->giveRoleTo($this->makeRoles($this->argument('roles')));
        $this->info("Profile `{$profile->name}` created");
    }

    /**
     * @param  $string
     * @return null
     */
    protected function makePermissions($string = null)
    {
        if (empty($string)) {
            return;
        }
        $permissionClass = app(PermissionContract::class);
        $permissions     = explode('|', $string);
        $models          = [];
        foreach ($permissions as $permission) {
            $models[] = $permissionClass::findOrCreate(trim($permission), $this->argument('guard'));
        }
        return collect($models);
    }

    /**
     * @param  $string
     * @return null
     */
    protected function makeRoles($string = null)
    {
        if (empty($string)) {
            return;
        }
        $roleClass = app(RoleContract::class);
        $roles     = explode('|', $string);
        $models    = [];
        foreach ($roles as $role) {
            $models[] = $roleClass::findOrCreate(trim($role), $this->argument('guard'));
        }
        return collect($models);
    }
}
