<?php
namespace Laravel\Acl\Middlewares;

use Closure;
use Spatie\Permission\Middlewares\RoleOrPermissionMiddleware as BaseRoleOrPermissionMiddleware;

class RoleOrPermissionMiddleware extends BaseRoleOrPermissionMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @param $roleOrPermission
     */
    public function handle($request, Closure $next, $roleOrPermission)
    {
        return parent::handle($request, $next, $roleOrPermission);
    }
}
