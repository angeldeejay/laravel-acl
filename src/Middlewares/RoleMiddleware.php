<?php
namespace Laravel\Acl\Middlewares;

use Closure;
use Spatie\Permission\Middlewares\RoleMiddleware as BaseRoleMiddleware;

class RoleMiddleware extends BaseRoleMiddleware
{
    /**
     * @param  $request
     * @param  Closure    $next
     * @param  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        return parent::handle($request, $next, $role);
    }
}
