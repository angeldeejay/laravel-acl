<?php
namespace Laravel\Acl\Middlewares;

use Closure;
use Illuminate\Support\Facades\Auth;
use Laravel\Acl\Exceptions\UnauthorizedException;

class ProfileOrRoleOrPermissionMiddleware
{
    /**
     * @param  $request
     * @param  Closure                      $next
     * @param  $profileOrRoleOrPermission
     * @return mixed
     */
    public function handle($request, Closure $next, $profileOrRoleOrPermission)
    {
        if (Auth::guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $profilesOrRolesOrPermissions = is_array($profileOrRoleOrPermission)
            ? $profileOrRoleOrPermission
            : explode('|', $profileOrRoleOrPermission);

        if (!Auth::user()->hasAnyProfile($profilesOrRolesOrPermissions) &&
            !Auth::user()->hasAnyRole($profilesOrRolesOrPermissions) &&
            !Auth::user()->hasAnyPermission($profilesOrRolesOrPermissions)) {
            throw UnauthorizedException::forProfilesOrRolesOrPermissions($profilesOrRolesOrPermissions);
        }

        return $next($request);
    }
}
