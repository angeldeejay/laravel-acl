<?php
namespace Laravel\Acl\Middlewares;

use Closure;
use Illuminate\Support\Facades\Auth;
use Laravel\Acl\Exceptions\UnauthorizedException;

class ProfileMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @param $profile
     * @return mixed
     */
    public function handle($request, Closure $next, $profile)
    {
        if (Auth::guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $profiles = is_array($profile)
            ? $profile
            : explode('|', $profile);

        if (!Auth::user()->hasAnyProfile($profiles)) {
            throw UnauthorizedException::forProfiles($profiles);
        }

        return $next($request);
    }
}
