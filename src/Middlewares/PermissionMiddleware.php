<?php
namespace Laravel\Acl\Middlewares;

use Closure;
use Spatie\Permission\Middlewares\PermissionMiddleware as BasePermissionMiddleware;

class PermissionMiddleware extends BasePermissionMiddleware
{
    /**
     * @param  $request
     * @param  Closure       $next
     * @param  $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        return parent::handle($request, $next, $permission);
    }
}
