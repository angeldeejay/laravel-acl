<?php
namespace Laravel\Acl;

use Laravel\Acl\Contracts\Role;
use Illuminate\Cache\CacheManager;
use Illuminate\Support\Collection;
use Laravel\Acl\Contracts\Permission;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Access\Authorizable;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;
use Spatie\Permission\PermissionRegistrar as BasePermissionRegistrar;

class PermissionRegistrar extends BasePermissionRegistrar
{
    /**
     * @var string
     */
    protected $profileClass;

    /**
     * PermissionRegistrar constructor.
     *
     * @param \Illuminate\Contracts\Auth\Access\Gate $gate
     * @param \Illuminate\Cache\CacheManager         $cacheManager
     */
    public function __construct(Gate $gate, CacheManager $cacheManager)
    {
        $this->profileClass = config('permission.models.profile');
        parent::__construct($gate, $cacheManager);
    }

    /**
     * Get the permissions based on the passed params.
     *
     * @param  array                            $params
     * @return \Illuminate\Support\Collection
     */
    public function getPermissions(array $params = []): Collection
    {
        if ($this->permissions === null) {
            $this->permissions = $this->cache->remember(self::$cacheKey, self::$cacheExpirationTime, function () {
                return $this->getPermissionClass()
                            ->with('roles')
                            ->with('profiles')
                            ->get();
            });
        }

        $permissions = clone $this->permissions;

        foreach ($params as $attr => $value) {
            $permissions = $permissions->where($attr, $value);
        }

        return $permissions;
    }

    /**
     * Get an instance of the profile class.
     *
     * @return \Laravel\Acl\Contracts\Profile
     */
    public function getProfileClass(): Role
    {
        return app($this->profileClass);
    }

    /**
     * Register the permission check method on the gate.
     *
     * @return bool
     */
    public function registerPermissions(): bool
    {
        $this->gate->before(function (Authorizable $user, string $ability) {
            try {
                if (method_exists($user, 'hasPermissionTo')) {
                    return $user->hasPermissionTo($ability) ?: null;
                }
            } catch (PermissionDoesNotExist $e) {
            }
        });

        return true;
    }
}
