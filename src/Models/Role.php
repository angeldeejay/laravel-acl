<?php
namespace Laravel\Acl\Models;

use Laravel\Acl\Traits\HasProfiles;
use Laravel\Acl\Traits\HasPermissions;
use Spatie\Permission\Models\Role as BaseRole;
use Laravel\Acl\Contracts\Role as RoleContract;
use Laravel\Acl\Traits\RefreshesPermissionCache;

class Role extends BaseRole implements RoleContract
{
    use HasPermissions, HasProfiles, RefreshesPermissionCache;

    /**
     * A role can be applied to profiles.
     */
    public function profiles(): BelongsToMany
    {
        return $this->belongsToMany(
            config('role.models.profile'),
            config('role.table_names.profile_has_roles'),
            'role_id',
            'profile_id'
        );
    }
}
