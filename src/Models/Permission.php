<?php
namespace Laravel\Acl\Models;

use Laravel\Acl\Traits\HasProfiles;
use Laravel\Acl\Traits\RefreshesPermissionCache;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Permission\Models\Permission as BasePermission;
use Laravel\Acl\Contracts\Permission as PermissionContract;

class Permission extends BasePermission implements PermissionContract
{
    use HasProfiles, RefreshesPermissionCache;

    /**
     * A permission can be applied to profiles.
     */
    public function profiles(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.profile'),
            config('permission.table_names.profile_has_permissions'),
            'permission_id',
            'profile_id'
        );
    }
}
