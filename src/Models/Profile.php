<?php
namespace Spatie\Permission\Models;

use Spatie\Permission\Guard;
use Laravel\Acl\Traits\HasRoles;
use Laravel\Acl\Traits\HasPermissions;
use Illuminate\Database\Eloquent\Model;
use Laravel\Acl\Exceptions\ProfileDoesNotExist;
use Laravel\Acl\Exceptions\ProfileAlreadyExists;
use Spatie\Permission\Exceptions\GuardDoesNotMatch;
use Spatie\Permission\Models\Profile as BaseProfile;
use Laravel\Acl\Contracts\Profile as ProfileContract;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Profile extends BaseProfile implements ProfileContract
{
    use HasPermissions, HasRoles, HasRefreshesPermissionCache;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');
        parent::__construct($attributes);
        $this->setTable(config('permission.table_names.profiles'));
    }

    /**
     * @param array $attributes
     */
    public static function create(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? Guard::getDefaultName(static::class);
        if (static::where('name', $attributes['name'])->where('guard_name', $attributes['guard_name'])->first()) {
            throw ProfileAlreadyExists::create($attributes['name'], $attributes['guard_name']);
        }
        if (isNotLumen() && app()::VERSION < '5.4') {
            return parent::create($attributes);
        }
        return static::query()->create($attributes);
    }

    /**
     * @param  int          $id
     * @param  $guardName
     * @return mixed
     */
    public static function findById(int $id, $guardName = null): ProfileContract
    {
        $guardName = $guardName ?? Guard::getDefaultName(static::class);
        $profile   = static::where('id', $id)->where('guard_name', $guardName)->first();
        if (!$profile) {
            throw ProfileDoesNotExist::withId($id);
        }
        return $profile;
    }

    /**
     * Find a profile by its name and guard name.
     *
     * @param  string                                                             $name
     * @param  string|null                                                        $guardName
     * @throws \Laravel\Acl\Exceptions\ProfileDoesNotExist
     * @return \Laravel\Acl\Contracts\Profile|\Spatie\Permission\Models\Profile
     */
    public static function findByName(string $name, $guardName = null): ProfileContract
    {
        $guardName = $guardName ?? Guard::getDefaultName(static::class);
        $profile   = static::where('name', $name)->where('guard_name', $guardName)->first();
        if (!$profile) {
            throw ProfileDoesNotExist::named($name);
        }
        return $profile;
    }

    /**
     * Find or create profile by its name (and optionally guardName).
     *
     * @param  string                           $name
     * @param  string|null                      $guardName
     * @return \Laravel\Acl\Contracts\Profile
     */
    public static function findOrCreate(string $name, $guardName = null): ProfileContract
    {
        $guardName = $guardName ?? Guard::getDefaultName(static::class);
        $profile   = static::where('name', $name)->where('guard_name', $guardName)->first();
        if (!$profile) {
            return static::query()->create(['name' => $name, 'guard_name' => $guardName]);
        }
        return $profile;
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param  string|Permission                                 $permission
     * @throws \Spatie\Permission\Exceptions\GuardDoesNotMatch
     * @return bool
     */
    public function hasPermissionTo($permission): bool
    {
        $permissionClass = $this->getPermissionClass();
        if (is_string($permission)) {
            $permission = $permissionClass->findByName($permission, $this->getDefaultGuardName());
        }
        if (is_int($permission)) {
            $permission = $permissionClass->findById($permission, $this->getDefaultGuardName());
        }
        if (!$this->getGuardNames()->contains($permission->guard_name)) {
            throw GuardDoesNotMatch::create($permission->guard_name, $this->getGuardNames());
        }
        return $this->permissions->contains('id', $permission->id);
    }

    /**
     * A profile may be given various permissions.
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.permission'),
            config('permission.table_names.profile_has_permissions'),
            'profile_id',
            'permission_id'
        );
    }

    /**
     * A profile may be given various roles.
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.role'),
            config('permission.table_names.profile_has_roles'),
            'profile_id',
            'role_id'
        );
    }

    /**
     * A profile belongs to some users of the model associated with its guard.
     */
    public function users(): MorphToMany
    {
        return $this->morphedByMany(
            getModelForGuard($this->attributes['guard_name']),
            'model',
            config('permission.table_names.model_has_profiles'),
            'profile_id',
            config('permission.column_names.model_morph_key')
        );
    }

    /**
     * Get the current cached permissions.
     */
    protected static function getPermissions(array $params = []): Collection
    {
        return app(PermissionRegistrar::class)->getPermissions($params);
    }
}
