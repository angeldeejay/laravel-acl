<?php
namespace Laravel\Acl\Traits;

use Spatie\Permission\Traits\RefreshesPermissionCache as BaseRefreshesPermissionCache;

trait RefreshesPermissionCache
{
    use BaseRefreshesPermissionCache;
}
