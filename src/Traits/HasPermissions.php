<?php
namespace Laravel\Acl\Traits;

use Spatie\Permission\Traits\HasPermissions as BaseHasPermissions;

trait HasPermissions
{
    use BaseHasPermissions;
}
