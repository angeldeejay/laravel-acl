<?php
namespace Laravel\Acl\Traits;

use Illuminate\Support\Collection;
use Laravel\Acl\Contracts\Profile;
use Laravel\Acl\PermissionRegistrar;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasProfiles
{
    use HasPermissions, HasRoles;

    /**
     * @var mixed
     */
    private $profileClass;

    /**
     * Assign the given profile to the model.
     *
     * @param  array|string|\Laravel\Acl\Contracts\Profile ...$profiles
     * @return $this
     */
    public function assignProfile(...$profiles)
    {
        $profiles = collect($profiles)
            ->flatten()
            ->map(function ($profile) {
                if (empty($profile)) {
                    return false;
                }
                return $this->getStoredProfile($profile);
            })
            ->filter(function ($profile) {
                return $profile instanceof Profile;
            })
            ->each(function ($profile) {
                $this->ensureModelSharesGuard($profile);
            })
            ->map->id
            ->all();
        $model = $this->getModel();
        if ($model->exists) {
            $this->profiles()->sync($profiles, false);
            $model->load('profiles');
        } else {
            $class = \get_class($model);
            $class::saved(
                function ($object) use ($profiles, $model) {
                    /**
                     * @var mixed
                     */
                    static $modelLastFiredOn;
                    if ($modelLastFiredOn !== null && $modelLastFiredOn === $model) {
                        return;
                    }
                    $object->profiles()->sync($profiles, false);
                    $object->load('profiles');
                    $modelLastFiredOn = $object;
                });
        }
        $this->forgetCachedPermissions();
        return $this;
    }

    /**
     * @return null
     */
    public static function bootHasProfiles()
    {
        static::deleting(function ($model) {
            if (method_exists($model, 'isForceDeleting') && !$model->isForceDeleting()) {
                return;
            }
            $model->profiles()->detach();
        });
    }

    /**
     * Return all permissions directly coupled to the model.
     */
    public function getDirectPermissions(): Collection
    {
        return $this->permissions;
    }

    /**
     * @return mixed
     */
    public function getProfileClass()
    {
        if (!isset($this->profileClass)) {
            $this->profileClass = app(PermissionRegistrar::class)->getProfileClass();
        }
        return $this->profileClass;
    }

    /**
     * @return mixed
     */
    public function getProfileNames(): Collection
    {
        return $this->profiles->pluck('name');
    }

    /**
     * Determine if the model has all of the given profile(s).
     *
     * @param  string|\Laravel\Acl\Contracts\Profile|\Illuminate\Support\Collection $profiles
     * @return bool
     */
    public function hasAllProfiles($profiles): bool
    {
        if (is_string($profiles) && false !== strpos($profiles, '|')) {
            $profiles = $this->convertPipeToArray($profiles);
        }
        if (is_string($profiles)) {
            return $this->profiles->contains('name', $profiles);
        }
        if ($profiles instanceof Profile) {
            return $this->profiles->contains('id', $profiles->id);
        }
        $profiles = collect()->make($profiles)->map(function ($profile) {
            return $profile instanceof Profile ? $profile->name : $profile;
        });
        return $profiles->intersect($this->getProfileNames()) == $profiles;
    }

    /**
     * Determine if the model has any of the given profile(s).
     *
     * @param  string|array|\Laravel\Acl\Contracts\Profile|\Illuminate\Support\Collection $profiles
     * @return bool
     */
    public function hasAnyProfile($profiles): bool
    {
        return $this->hasProfile($profiles);
    }

    /**
     * Determine if the model has (one of) the given profile(s).
     *
     * @param  string|int|array|\Laravel\Acl\Contracts\Profile|\Illuminate\Support\Collection $profiles
     * @return bool
     */
    public function hasProfile($profiles): bool
    {
        if (is_string($profiles) && false !== strpos($profiles, '|')) {
            $profiles = $this->convertPipeToArray($profiles);
        }
        if (is_string($profiles)) {
            return $this->profiles->contains('name', $profiles);
        }
        if (is_int($profiles)) {
            return $this->profiles->contains('id', $profiles);
        }
        if ($profiles instanceof Profile) {
            return $this->profiles->contains('id', $profiles->id);
        }
        if (is_array($profiles)) {
            foreach ($profiles as $profile) {
                if ($this->hasProfile($profile)) {
                    return true;
                }
            }
            return false;
        }
        return $profiles->intersect($this->profiles)->isNotEmpty();
    }

    /**
     * A model may have multiple profiles.
     */
    public function profiles(): MorphToMany
    {
        return $this->morphToMany(
            config('permission.models.profile'),
            'model',
            config('permission.table_names.model_has_profiles'),
            config('permission.column_names.model_morph_key'),
            'profile_id'
        );
    }

    /**
     * Revoke the given profile from the model.
     *
     * @param string|\Laravel\Acl\Contracts\Profile $profile
     */
    public function removeProfile($profile)
    {
        $this->profiles()->detach($this->getStoredProfile($profile));
        $this->load('profiles');
        return $this;
    }

    /**
     * Scope the model query to certain profiles only.
     *
     * @param  \Illuminate\Database\Eloquent\Builder                                      $query
     * @param  string|array|\Laravel\Acl\Contracts\Profile|\Illuminate\Support\Collection $profiles
     * @param  string                                                                     $guard
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProfile(Builder $query, $profiles, $guard = null): Builder
    {
        if ($profiles instanceof Collection) {
            $profiles = $profiles->all();
        }
        if (!is_array($profiles)) {
            $profiles = [$profiles];
        }
        $profiles = array_map(function ($profile) use ($guard) {
            if ($profile instanceof Profile) {
                return $profile;
            }
            $method = is_numeric($profile) ? 'findById' : 'findByName';
            $guard  = $guard ?: $this->getDefaultGuardName();
            return $this->getProfileClass()->{$method}($profile, $guard);
        }, $profiles);
        return $query->whereHas('profiles', function ($query) use ($profiles) {
            $query->where(function ($query) use ($profiles) {
                foreach ($profiles as $profile) {
                    $query->orWhere(config('permission.table_names.profiles') . '.id', $profile->id);
                }
            });
        });
    }

    /**
     * Remove all current profiles and set the given ones.
     *
     * @param  array|\Laravel\Acl\Contracts\Profile|string ...$profiles
     * @return $this
     */
    public function syncProfiles(...$profiles)
    {
        $this->profiles()->detach();
        return $this->assignProfile($profiles);
    }

    /**
     * @param  string  $pipeString
     * @return mixed
     */
    protected function convertPipeToArray(string $pipeString)
    {
        $pipeString = trim($pipeString);
        if (strlen($pipeString) <= 2) {
            return $pipeString;
        }
        $quoteCharacter = substr($pipeString, 0, 1);
        $endCharacter   = substr($quoteCharacter, -1, 1);
        if ($quoteCharacter !== $endCharacter) {
            return explode('|', $pipeString);
        }
        if (!in_array($quoteCharacter, ["'", '"'])) {
            return explode('|', $pipeString);
        }
        return explode('|', trim($pipeString, $quoteCharacter));
    }

    /**
     * @param  $profile
     * @return mixed
     */
    protected function getStoredProfile($profile): Profile
    {
        $profileClass = $this->getProfileClass();
        if (is_numeric($profile)) {
            return $profileClass->findById($profile, $this->getDefaultGuardName());
        }
        if (is_string($profile)) {
            return $profileClass->findByName($profile, $this->getDefaultGuardName());
        }
        return $profile;
    }
}
