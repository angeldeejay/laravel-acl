<?php
namespace Laravel\Acl\Traits;

use Spatie\Permission\Traits\HasRoles as BaseHasRoles;

trait HasRoles
{
    use BaseHasRoles;
}
