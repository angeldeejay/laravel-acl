<?php
namespace Laravel\Acl;

use Illuminate\Routing\Route;
use Illuminate\Support\Collection;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Laravel\Acl\Contracts\Role as RoleContract;
use Laravel\Acl\Contracts\Profile as ProfileContract;
use Laravel\Acl\Contracts\Permission as PermissionContract;
use Spatie\Permission\PermissionServiceProvider as BasePermissionServiceProvider;
use Spatie\Permission\PermissionRegistrar as BasePermissionRegistrar;

class PermissionServiceProvider extends BasePermissionServiceProvider
{
    /**
     * @param  BasePermissionRegistrar $permissionLoader
     * @param  Filesystem          $filesystem
     * @return mixed
     */
    public function boot(BasePermissionRegistrar $permissionLoader, Filesystem $filesystem)
    {
        if (isNotLumen()) {
            $this->publishes([__DIR__ . '/../config/permission.php' => config_path('permission.php')], 'config');
            $this->publishes([__DIR__ . '/../database/migrations/create_permission_tables.php.stub' => $this->getMigrationFileName($filesystem)], 'migrations');
            if (app()->version() >= '5.5') {
                $this->registerMacroHelpers();
            }
        }
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\CacheReset::class,
                Commands\CreateProfile::class,
                \Spatie\Permission\Commands\CreateRole::class,
                \Spatie\Permission\Commands\CreatePermission::class,
            ]);
        }
        $this->registerModelBindings();
        $permissionLoader->registerPermissions();
        $this->app->singleton(PermissionRegistrar::class, function ($app) use ($permissionLoader) {
            return $permissionLoader;
        });
    }

    public function register()
    {
        if (isNotLumen()) {
            $this->mergeConfigFrom(__DIR__ . '/../config/permission.php', 'permission');
        }
        $this->registerBladeExtensions();
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param  Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');
        return Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path . '*_create_permission_tables.php');
            })
            ->push($this->app->databasePath() . "/migrations/{$timestamp}_create_permission_tables.php")
            ->first();
    }

    protected function registerBladeExtensions()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            // Roles
            $bladeCompiler->directive('role', function ($arguments) {
                list($role, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasRole({$role})): ?>";
            });
            $bladeCompiler->directive('elserole', function ($arguments) {
                list($role, $guard) = explode(',', $arguments . ',');
                return "<?php elseif(auth({$guard})->check() && auth({$guard})->user()->hasRole({$role})): ?>";
            });
            $bladeCompiler->directive('endrole', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('hasrole', function ($arguments) {
                list($role, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasRole({$role})): ?>";
            });
            $bladeCompiler->directive('endhasrole', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('hasanyrole', function ($arguments) {
                list($roles, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasAnyRole({$roles})): ?>";
            });
            $bladeCompiler->directive('endhasanyrole', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('hasallroles', function ($arguments) {
                list($roles, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasAllRoles({$roles})): ?>";
            });
            $bladeCompiler->directive('endhasallroles', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('unlessrole', function ($arguments) {
                list($role, $guard) = explode(',', $arguments . ',');
                return "<?php if(!auth({$guard})->check() || ! auth({$guard})->user()->hasRole({$role})): ?>";
            });
            $bladeCompiler->directive('endunlessrole', function () {
                return '<?php endif; ?>';
            });
            // Profiles
            $bladeCompiler->directive('profile', function ($arguments) {
                list($profile, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasRole({$profile})): ?>";
            });
            $bladeCompiler->directive('elseprofile', function ($arguments) {
                list($profile, $guard) = explode(',', $arguments . ',');
                return "<?php elseif(auth({$guard})->check() && auth({$guard})->user()->hasRole({$profile})): ?>";
            });
            $bladeCompiler->directive('endprofile', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('hasprofile', function ($arguments) {
                list($profile, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasRole({$profile})): ?>";
            });
            $bladeCompiler->directive('endhasprofile', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('hasanyprofile', function ($arguments) {
                list($profiles, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasAnyRole({$profiles})): ?>";
            });
            $bladeCompiler->directive('endhasanyprofile', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('hasallprofiles', function ($arguments) {
                list($profiles, $guard) = explode(',', $arguments . ',');
                return "<?php if(auth({$guard})->check() && auth({$guard})->user()->hasAllRoles({$profiles})): ?>";
            });
            $bladeCompiler->directive('endhasallprofiles', function () {
                return '<?php endif; ?>';
            });
            $bladeCompiler->directive('unlessprofile', function ($arguments) {
                list($profile, $guard) = explode(',', $arguments . ',');
                return "<?php if(!auth({$guard})->check() || ! auth({$guard})->user()->hasRole({$profile})): ?>";
            });
            $bladeCompiler->directive('endunlessprofile', function () {
                return '<?php endif; ?>';
            });
        });
    }

    /**
     * @return mixed
     */
    protected function registerMacroHelpers()
    {
        Route::macro('profile', function ($profiles = []) {
            if (!is_array($profiles)) {
                $profiles = [$profiles];
            }
            $profiles = implode('|', $profiles);
            $this->middleware("profile:$profiles");
            return $this;
        });
        Route::macro('role', function ($roles = []) {
            if (!is_array($roles)) {
                $roles = [$roles];
            }
            $roles = implode('|', $roles);
            $this->middleware("role:$roles");
            return $this;
        });
        Route::macro('permission', function ($permissions = []) {
            if (!is_array($permissions)) {
                $permissions = [$permissions];
            }
            $permissions = implode('|', $permissions);
            $this->middleware("permission:$permissions");
            return $this;
        });
    }

    protected function registerModelBindings()
    {
        $config = $this->app->config['permission.models'];
        $this->app->bind(PermissionContract::class, $config['permission']);
        $this->app->bind(RoleContract::class, $config['role']);
        $this->app->bind(ProfileContract::class, $config['profile']);
    }
}
