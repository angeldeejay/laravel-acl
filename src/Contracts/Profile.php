<?php
namespace Laravel\Acl\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface Profile
{
    /**
     * Find a profile by its id and guard name.
     *
     * @param  int                                           $id
     * @param  string|null                                   $guardName
     * @throws \Laravel\Acl\Exceptions\ProfileDoesNotExist
     * @return \Laravel\Acl\Contracts\Profile
     */
    public static function findById(int $id, $guardName): self;

    /**
     * Find a profile by its name and guard name.
     *
     * @param  string                                        $name
     * @param  string|null                                   $guardName
     * @throws \Laravel\Acl\Exceptions\ProfileDoesNotExist
     * @return \Laravel\Acl\Contracts\Profile
     */
    public static function findByName(string $name, $guardName): self;

    /**
     * Find or create a profile by its name and guard name.
     *
     * @param  string                           $name
     * @param  string|null                      $guardName
     * @return \Laravel\Acl\Contracts\Profile
     */
    public static function findOrCreate(string $name, $guardName): self;

    /**
     * Determine if the user may perform the given permission.
     *
     * @param  string|\Laravel\Acl\Contracts\Permission $permission
     * @return bool
     */
    public function hasPermissionTo($permission): bool;

    /**
     * A profile may be given various permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions(): BelongsToMany;

    /**
     * A profile may be given various roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany;
}
