<?php
namespace Laravel\Acl\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Permission\Contracts\Role as BaseRoleContract;

interface Role extends BaseRoleContract
{
    /**
     * A permission can be applied to profiles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profiles(): BelongsToMany;
}
