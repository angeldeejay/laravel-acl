<?php
namespace Laravel\Acl\Exceptions;

use InvalidArgumentException;

class ProfileDoesNotExist extends InvalidArgumentException
{
    /**
     * @param string $profileName
     */
    public static function named(string $profileName)
    {
        return new static("There is no profile named `{$profileName}`.");
    }

    /**
     * @param int $profileId
     */
    public static function withId(int $profileId)
    {
        return new static("There is no profile with id `{$profileId}`.");
    }
}
