<?php
namespace Laravel\Acl\Exceptions;

use Spatie\Permission\Exceptions\UnauthorizedException as BaseUnauthorizedException;

class UnauthorizedException extends BaseUnauthorizedException
{
    /**
     * @var array
     */
    private $requiredProfiles = [];

    /**
     * @param  array   $profiles
     * @return mixed
     */
    public static function forProfiles(array $profiles): self
    {
        $message = 'User does not have the right profiles.';
        if (config('permission.display_profile_in_exception')) {
            $permStr = implode(', ', $profiles);
            $message = 'User does not have the right profiles. Necessary profiles are ' . $permStr;
        }
        $exception = new static(403, $message, null, []);

        $exception->requiredProfiles = $profiles;
        return $exception;
    }

    /**
     * @param  array   $profilesOrRolesOrPermissions
     * @return mixed
     */
    public static function forProfilesOrRolesOrPermissions(array $profilesOrRolesOrPermissions): self
    {
        $message = 'User does not have any of the necessary access rights.';
        if (config('permission.display_permission_in_exception') && config('permission.display_role_in_exception') && config('permission.display_profile_in_exception')) {
            $permStr = implode(', ', $profilesOrRolesOrPermissions);
            $message = 'User does not have the right permissions/roles/profiles. Necessary permissions/roles/profiles are ' . $permStr;
        }
        $exception = new static(403, $message, null, []);

        $exception->requiredPermissions = $profilesOrRolesOrPermissions;
        return $exception;
    }

    /**
     * @param  array   $rolesOrPermissions
     * @return mixed
     */
    public static function forRolesOrPermissions(array $rolesOrPermissions): self
    {
        $message = 'User does not have any of the necessary access rights.';
        if (config('permission.display_permission_in_exception') && config('permission.display_role_in_exception')) {
            $permStr = implode(', ', $rolesOrPermissions);
            $message = 'User does not have the right permissions/roles. Necessary permissions/roles are ' . $permStr;
        }
        $exception = new static(403, $message, null, []);

        $exception->requiredPermissions = $rolesOrPermissions;
        return $exception;
    }

    /**
     * @return mixed
     */
    public function getRequiredProfiles(): array
    {
        return $this->requiredProfiles;
    }
}
