<?php
namespace Laravel\Acl\Exceptions;

use InvalidArgumentException;

class ProfileAlreadyExists extends InvalidArgumentException
{
    /**
     * @param string $profileName
     * @param string $guardName
     */
    public static function create(string $profileName, string $guardName)
    {
        return new static("A profile `{$profileName}` already exists for guard `{$guardName}`.");
    }
}
